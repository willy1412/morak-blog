const pkg = require('./package')

module.exports = {
  mode: 'universal',

  /*
  ** Headers of the page
  */
  head: {
    title: "Blog | Modal Rakyat Indonesia | Platform P2P Lending",
    meta: [
      { charset: 'utf-8' },
      { name: 'viewport', content: 'width=device-width, initial-scale=1' },
      { hid: 'description', name: 'description', content: pkg.description },
      { name: 'keywords', content: 'fintech, pinjaman, investasi, pendanaan, modal, rakyat,  indonesia, finansial,  teknologi, peer-to-peer, p2p, lending, terpercaya, blog, umkm'},
      { hid: 'ogTitle', name: 'og:title', content: "Modal Rakyat Blog" },
      { hid: 'ogType', name: 'og:type', content: "blog" },
      { hid: 'ogUrl', name: 'og:url', content: "https://www.modalrakyat.id/blog" },
      { hid: 'ogSiteName', name: 'og:site_name', content: "Modal Rakyat Blog" },
      { hid: 'fbPage', name: 'fb:page_id', content: "173332989926056" },
    ],
    script: [
      { src:  'https://connect.facebook.net/en_US/all.js' }
    ],
    link: [
      { rel: 'icon', type: 'image/x-icon', href: '/favicon.ico' }
    ]
  },
  /*
  ** Customize the progress-bar color
  */
  loading: { color: '#1c8cef' },

  /*
  ** Global CSS
  */
  css: [
    '@/assets/navbar.scss',
    '@/assets/footer.scss',
    '@/assets/index.scss',
    '@/assets/_uri.scss',
  ],

  /*
  ** Plugins to load before mounting the App
  */
  plugins: [
    'plugins/social.js',
    'plugins/facebook.js'
  ],

  /*
  ** Nuxt.js modules
  */
  modules: [
    // Doc: https://github.com/nuxt-community/axios-module#usage
    '@nuxtjs/axios',
    // Doc: https://bootstrap-vue.js.org/docs/
    'bootstrap-vue/nuxt',
    '@nuxtjs/font-awesome',

  ],
  /*
  ** Axios module configuration
  */
  axios: {
    // See https://github.com/nuxt-community/axios-module#options
  },

  /*
  ** Build configuration
  */
  build: {
    /*
    ** You can extend webpack config here
    */
    extend(config, ctx) {
      // Run ESLint on save
      if (ctx.isDev && ctx.isClient) {
        config.module.rules.push({
          enforce: 'pre',
          test: /\.(js|vue)$/,
          loader: 'eslint-loader',
          exclude: /(node_modules)/
        })
      }
    }
  },

  router: {
    base: '/blog/'
  }
}
