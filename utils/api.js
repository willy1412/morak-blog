import axios from 'axios';

const API_URL = process.env.API_URL;

const getPostList = () => axios.post("https://p2p.modalrakyat.id/api/blog/get_list");
const getPopular = () => axios.post("https://p2p.modalrakyat.id/api/blog/get_artikel_populer");
const getPostByUri = (uri,payload) => axios.post("https://p2p.modalrakyat.id/api/blog/get_blog_by_uri", { uri: uri });

export default {
    getPostList,
    getPopular,
    getPostByUri
  };